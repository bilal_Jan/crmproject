﻿using MvcAppCRM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList.Mvc;
using PagedList;


namespace MvcAppCRM.Controllers
{
    public class MCustomerController : Controller
    {
        TestCRMEntities db = new TestCRMEntities();
        // GET: /MCustomer/

        public ActionResult Index(int? page)
        {
            int pagenumber = page ?? 1;
            int pagesize = 4;

            var data = db.Customers.OrderBy(x => x.Name).ToPagedList(pagenumber, pagesize);

            return View(data);
        }


        public ActionResult Create()
        {

            return View();

        }

        [HttpPost]
        public ActionResult Create(Customer cost)
        {
            db.Customers.Add(cost);
            db.SaveChanges();

            return RedirectToAction("Index", "MCustomer");

        }

        public ActionResult Edit(int id)
        {
            var cus = db.Customers.Where(m => m.Id == id).FirstOrDefault();
            return View(cus);

        }

        [HttpPost]
        public ActionResult Edit(Customer cus)
        {
            
            

                var us = db.Customers.FirstOrDefault(x => x.Id == cus.Id);
                if (us != null)
                {

                    us.Name = cus.Name;
                    us.Address = cus.Address;
                    us.Deleted = cus.Deleted;
                    us.Created = cus.Created;
                    us.CreatedBy = cus.CreatedBy;
                    us.Modified = cus.Modified;
                    us.ModifiedBy = cus.ModifiedBy;
                    
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

            
            return View(cus);

        }


        public ActionResult Delete(int id)
        {
            Customer c = db.Customers.Find(id);
            if (c == null)
            {

                return HttpNotFound();

            }

            return View(c);

        }

        [HttpPost,ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id = 0)
        {

            Customer c = db.Customers.Find(id);
            db.Customers.Remove(c);
            db.SaveChanges();


            return RedirectToAction("Index");
        }
    }
}
