//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MvcAppCRM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Location2Unit
    {
        public int Id { get; set; }
        public int UnitId { get; set; }
        public int LocationId { get; set; }
        public System.DateTime Deleted { get; set; }
        public Nullable<System.DateTime> Created { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> Modified { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
    
        public virtual Location Location { get; set; }
        public virtual Unit Unit { get; set; }
    }
}
